const { Router } = require("express");


const home = Router();

home.get("/", (req,res) => {
    return res.render(`index.html`);
});

module.exports = home;

