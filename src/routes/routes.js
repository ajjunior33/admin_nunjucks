const {Router} = require('express');

const Home = require('./home.routes');

const route = Router();

route.use('/', Home);


module.exports = route;