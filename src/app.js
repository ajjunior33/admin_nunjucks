const express = require('express');

const app = express();

const router = require('./routes/routes');

app.use(express.static('public'));
app.use(express.urlencoded({extended:true}));
// Utilizando template Engine

const nunjucks = require("nunjucks");
nunjucks.configure("src/views", {
    express: app,
    noCache: true
}); 

app.use(router);



module.exports = app;